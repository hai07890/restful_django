from django.db import models
from django.contrib.auth.models import User
from datetime import datetime    

# Create your models here.

class User(models.Model):
    auth_user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    address = models.CharField(max_length = 500)
    phone_number = models.CharField(max_length = 30)
    email = models.CharField(max_length = 100, unique=True)

    class Meta:
        db_table = "user"

class Book(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length = 50)
    publish_date = models.DateTimeField()
    publish_company = models.CharField(max_length = 50)
    
    class Meta:
        db_table = "book"

class Author(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length = 50, null=True)
    pen_name = models.CharField(max_length = 50, unique=True)
    address = models.CharField(max_length = 50, null=True)
    phone_number = models.CharField(max_length = 30)
    email = models.CharField(max_length = 50)
    books = models.ManyToManyField(Book)
    
    class Meta:
        db_table = "author"
		
class BookCopy(models.Model):
    id = models.AutoField(primary_key=True)
    quality = models.CharField(max_length = 50, null=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "bookcopy"
    
class Borrow(models.Model):
    id = models.AutoField(primary_key=True)
    borrow_date = models.DateTimeField(default=datetime.now)
    time_period = models.DateTimeField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "borrow"
    
class ReturnBook(models.Model):
    borrow = models.OneToOneField(Borrow, primary_key=True, on_delete=models.CASCADE)
    changes_quality = models.CharField(max_length = 500)
    time = models.DateTimeField(default=datetime.now)
    
    class Meta:
        db_table = "returnbook"