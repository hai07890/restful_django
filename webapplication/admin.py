from django.contrib import admin
from .models import User, Author, Borrow, Book, ReturnBook, BookCopy
# Register your models here.

admin.site.register(User)
admin.site.register(Author)
admin.site.register(Borrow)
admin.site.register(Book)
admin.site.register(ReturnBook)
admin.site.register(BookCopy)
